 #!/bin/bash
./configure \
 PATH="$PATH:/home/common/lib/gsl-1.16-intel/bin" \
 CC="mpiicc -m64" CFLAGS=" -O3 -march=native -I${MKLROOT}/include/intel64/lp64 -I${MKLROOT}/include" \
 FC="mpiifort -m64" FCFLAGS="-O3 -I${MKLROOT}/include/intel64/lp64 -I${MKLROOT}/include" \
 GSL_CONFIG=/home/common/lib/gsl-1.16-intel/bin/gsl-config --with-gsl-prefix="/home/common/lib/gsl-1.16-intel/" \
 --with-libxc-prefix=$LIBXC_PREFIX \
 --with-blas="${MKLROOT}/lib/intel64/libmkl_blas95_lp64.a ${MKLROOT}/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_sequential.a -Wl,--end-group -lpthread -lm -ldl" \
 --with-lapack="${MKLROOT}/lib/intel64/libmkl_blas95_lp64.a ${MKLROOT}/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_sequential.a -Wl,--end-group -lpthread -lm -ldl" \
 --with-fft-lib=/home/common/lib/fftw-3.3.4-intel/lib/libfftw3.a
