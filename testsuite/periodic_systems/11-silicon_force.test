# -*- coding: utf-8 mode: shell-script -*-

Test       : Silicon force
Program    : octopus
TestGroups : short-run, periodic_systems
Enabled    : Yes

Precision : 1e-8

Input      : 11-silicon_force.01-gs.inp

match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

match ;  Total k-points    ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 8.0
match ;  Reduced k-points  ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 4.0
match ;  Space group         ; GREPFIELD(out, 'Space group', 4) ; 1.0
match ;  No. of symmetries   ; GREPFIELD(out, 'symmetries that can be used', 5) ; 1.0

Precision: 1.57e-06
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -31.3500567
Precision: 1.70e-07
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -33.95408826
Precision: 6.41e-08
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; 1.28296348
Precision: 9.15e-08
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 1.82973564
Precision: 4.40e-07
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -8.79367124
Precision: 7.76e-07
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -1.5528737000000001
Precision: 7.05e-08
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 14.095183519999999
Precision: 1.49e-07
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -2.97434266

Precision: 1.28e-15
match ;    Force 1 (x)      ; GREPFIELD(static/info, '1        Si', 3) ; -0.128161913
Precision: 4.09e-09
match ;    Force 1 (y)      ; GREPFIELD(static/info, '1        Si', 4) ; -0.0817190612
Precision: 3.25e-09
match ;    Force 1 (z)      ; GREPFIELD(static/info, '1        Si', 5) ; -0.0649022863
Precision: 6.57e-10
match ;    Force 2 (x)      ; GREPFIELD(static/info, '2        Si', 3) ; 0.0131363121
Precision: 3.12e-10
match ;    Force 2 (y)      ; GREPFIELD(static/info, '2        Si', 4) ; -0.00624989982
Precision: 5.97e-10
match ;    Force 2 (z)      ; GREPFIELD(static/info, '2        Si', 5) ; -0.0119357199
Precision: 6.36e-10
match ;    Force 3 (x)      ; GREPFIELD(static/info, '3        Si', 3) ; 0.0127127659
Precision: 4.74e-10
match ;    Force 3 (y)      ; GREPFIELD(static/info, '3        Si', 4) ; 0.00948960058
Precision: 4.69e-10
match ;    Force 3 (z)      ; GREPFIELD(static/info, '3        Si', 5) ; 0.00937317192
Precision: 7.75e-10
match ;    Force 4 (x)      ; GREPFIELD(static/info, '4        Si', 3) ; -0.015500182600000002
Precision: 3.05e-10
match ;    Force 4 (y)      ; GREPFIELD(static/info, '4        Si', 4) ; -0.00609626067
Precision: 9.58e-17
match ;    Force 4 (z)      ; GREPFIELD(static/info, '4        Si', 5) ; 0.009583596100000001
Precision: 3.02e-09
match ;    Force 5 (x)      ; GREPFIELD(static/info, '5        Si', 3) ; 0.0604170591
Precision: 6.99e-08
match ;    Force 5 (y)      ; GREPFIELD(static/info, '5        Si', 4) ; 0.13971354
Precision: 3.64e-09
match ;    Force 5 (z)      ; GREPFIELD(static/info, '5        Si', 5) ; 0.0727564782
Precision: 4.25e-11
match ;    Force 6 (x)      ; GREPFIELD(static/info, '6        Si', 3) ; 0.000849571451
Precision: 4.74e-10
match ;    Force 6 (y)      ; GREPFIELD(static/info, '6        Si', 4) ; 0.00948271483
Precision: 5.34e-10
match ;    Force 6 (z)      ; GREPFIELD(static/info, '6        Si', 5) ; -0.010687142900000002
Precision: 2.23e-09
match ;    Force 7 (x)      ; GREPFIELD(static/info, '7        Si', 3) ; 0.0445770415
Precision: 2.17e-09
match ;    Force 7 (y)      ; GREPFIELD(static/info, '7        Si', 4) ; -0.04341668060000001
Precision: 2.20e-09
match ;    Force 7 (z)      ; GREPFIELD(static/info, '7        Si', 5) ; 0.0439517145
Precision: 5.91e-10
match ;    Force 8 (x)      ; GREPFIELD(static/info, '8        Si', 3) ; 0.0118178812
Precision: 1.06e-09
match ;    Force 8 (y)      ; GREPFIELD(static/info, '8        Si', 4) ; -0.0212213953
Precision: 2.41e-09
match ;    Force 8 (z)      ; GREPFIELD(static/info, '8        Si', 5) ; -0.0481376659

Precision: 1.25e-01
match ;     k-point 1 (x)     ; GREPFIELD(static/info, '#k =       1', 7) ; 0.25
match ;     k-point 1 (y)     ; GREPFIELD(static/info, '#k =       1', 8) ; 0.25
match ;     k-point 1 (z)     ; GREPFIELD(static/info, '#k =       1', 9) ; 0.25
Precision: 1.18e-05
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.235543
Precision: 7.16e-16
match ;     Eigenvalue  8     ; GREPFIELD(static/info, '#k =       1', 3, 8) ; 0.071611
Precision: 2.61e-15
match ;     Eigenvalue 16     ; GREPFIELD(static/info, '#k =       1', 3, 16) ; 0.260605
Precision: 1.44e-05
match ;     Eigenvalue 17     ; GREPFIELD(static/info, '#k =       1', 3, 17) ; 0.287219

Precision: 1.25e-01
match ;     k-point 4 (x)     ; GREPFIELD(static/info, '#k =       4', 7) ; 0.25
match ;     k-point 4 (y)     ; GREPFIELD(static/info, '#k =       4', 8) ; 0.25
match ;     k-point 4 (z)     ; GREPFIELD(static/info, '#k =       4', 9) ; -0.25
Precision: 1.18e-05
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       4', 3, 1) ; -0.235767
Precision: 3.81e-05
match ;     Eigenvalue  8     ; GREPFIELD(static/info, '#k =       4', 3, 8) ; 0.07625300000000002
Precision: 1.29e-05
match ;     Eigenvalue 16     ; GREPFIELD(static/info, '#k =       4', 3, 16) ; 0.258762
Precision: 1.40e-04
match ;     Eigenvalue 17     ; GREPFIELD(static/info, '#k =       4', 3, 17) ; 0.28008
Precision: 1.25e-01
match ;     k-point 3 (x)     ; GREPFIELD(static/info, '#k =       3', 7) ; 0.25
Precision: 1.18e-05
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       3', 3, 1) ; -0.236386
Precision: 4.17e-05
match ;     Eigenvalue  8     ; GREPFIELD(static/info, '#k =       3', 3, 8) ; 0.083415
Precision: 1.17e-05
match ;     Eigenvalue 16     ; GREPFIELD(static/info, '#k =       3', 3, 16) ; 0.233143
Precision: 1.45e-05
match ;     Eigenvalue 17     ; GREPFIELD(static/info, '#k =       3', 3, 17) ; 0.289458

Precision: 1.25e-01
match ;     k-point 2 (x)     ; GREPFIELD(static/info, '#k =       2', 7) ; -0.25
match ;     k-point 2 (y)     ; GREPFIELD(static/info, '#k =       2', 8) ; 0.25
match ;     k-point 2 (z)     ; GREPFIELD(static/info, '#k =       2', 9) ; 0.25
Precision: 1.18e-05
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -0.235599
Precision: 3.62e-05
match ;     Eigenvalue  8     ; GREPFIELD(static/info, '#k =       2', 3, 8) ; 0.072483
Precision: 1.30e-05
match ;     Eigenvalue 16     ; GREPFIELD(static/info, '#k =       2', 3, 16) ; 0.260411
Precision: 1.42e-05
match ;     Eigenvalue 17     ; GREPFIELD(static/info, '#k =       2', 3, 17) ; 0.283939

Input      : 11-silicon_force.02-nlcc.inp

match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 2.84e-04
match ;    Force 1 (x)      ; GREPFIELD(static/info, '1        Si', 3) ; -0.118259808
Precision: 8.64e-04
match ;    Force 1 (y)      ; GREPFIELD(static/info, '1        Si', 4) ; -0.08465135780000001
Precision: 6.51e-04
match ;    Force 1 (z)      ; GREPFIELD(static/info, '1        Si', 5) ; -0.0583467098
Precision: 8.99e-05
match ;    Force 2 (x)      ; GREPFIELD(static/info, '2        Si', 3) ; 0.011280548000000001
Precision: 3.26e-05
match ;    Force 2 (y)      ; GREPFIELD(static/info, '2        Si', 4) ; -0.004260716580000001
Precision: 1.10e-05
match ;    Force 2 (z)      ; GREPFIELD(static/info, '2        Si', 5) ; -0.0103611993
Precision: 1.99e-04
match ;    Force 3 (x)      ; GREPFIELD(static/info, '3        Si', 3) ; 0.010587087500000002
Precision: 4.64e-05
match ;    Force 3 (y)      ; GREPFIELD(static/info, '3        Si', 4) ; 0.00825769779
Precision: 2.14e-04
match ;    Force 3 (z)      ; GREPFIELD(static/info, '3        Si', 5) ; 0.00719181529
Precision: 2.14e-05
match ;    Force 4 (x)      ; GREPFIELD(static/info, '4        Si', 3) ; -0.0137859985
Precision: 4.73e-05
match ;    Force 4 (y)      ; GREPFIELD(static/info, '4        Si', 4) ; -0.00451593449
Precision: 8.16e-05
match ;    Force 4 (z)      ; GREPFIELD(static/info, '4        Si', 5) ; 0.00811186593
Precision: 4.21e-04
match ;    Force 5 (x)      ; GREPFIELD(static/info, '5        Si', 3) ; 0.0588360958
Precision: 8.45e-04
match ;    Force 5 (y)      ; GREPFIELD(static/info, '5        Si', 4) ; 0.13715378
Precision: 1.19e-04
match ;    Force 5 (z)      ; GREPFIELD(static/info, '5        Si', 5) ; 0.071435514
Precision: 1.01e-04
match ;    Force 6 (x)      ; GREPFIELD(static/info, '6        Si', 3) ; -0.00139837116
Precision: 4.34e-04
match ;    Force 6 (y)      ; GREPFIELD(static/info, '6        Si', 4) ; 0.009169154430000001
Precision: 2.89e-04
match ;    Force 6 (z)      ; GREPFIELD(static/info, '6        Si', 5) ; -0.0105024008
Precision: 8.10e-05
match ;    Force 7 (x)      ; GREPFIELD(static/info, '7        Si', 3) ; 0.0414722146
Precision: 1.36e-04
match ;    Force 7 (y)      ; GREPFIELD(static/info, '7        Si', 4) ; -0.0410129237
Precision: 1.97e-05
match ;    Force 7 (z)      ; GREPFIELD(static/info, '7        Si', 5) ; 0.04115800620000001
Precision: 4.40e-04
match ;    Force 8 (x)      ; GREPFIELD(static/info, '8        Si', 3) ; 0.0110690613
Precision: 4.98e-04
match ;    Force 8 (y)      ; GREPFIELD(static/info, '8        Si', 4) ; -0.020632260200000002
Precision: 2.77e-04
match ;    Force 8 (z)      ; GREPFIELD(static/info, '8        Si', 5) ; -0.0486925022
Precision: 1.79e-08
match ;    Force Ion-ion   ; LINEFIELD(static/forces, 2, 6) ; -0.357264512
Precision: 8.73e-04
match ;    Force Local     ; LINEFIELD(static/forces, 2, 12) ; 0.352998356
Precision: 2.52e-04
match ;    Force NL        ; LINEFIELD(static/forces, 2, 15) ; -0.12049521
Precision: 2.22e-11
match ;    Force SCF       ; LINEFIELD(static/forces, 2, 24) ; -1.50616761e-11
Precision: 3.37e-04
match ;    Force NLCC      ; LINEFIELD(static/forces, 2, 27) ; 0.00650155779

Processors : 4
Input      : 11-silicon_force.03-nosym.inp

match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 1.79e-08
match ;     Force Ion-ion    ; LINEFIELD(static/forces, 2, 6) ; -0.357264512
Precision: 8.73e-04
match ;     Force Local      ; LINEFIELD(static/forces, 2, 12) ; 0.352998356
Precision: 2.52e-04
match ;     Force NL         ; LINEFIELD(static/forces, 2, 15) ; -0.12049521
Precision: 2.22e-11
match ;     Force SCF        ; LINEFIELD(static/forces, 2, 24) ; -1.50616706e-11
Precision: 3.37e-04
match ;     Force NLCC       ; LINEFIELD(static/forces, 2, 27) ; 0.00650155779
Precision: 2.84e-04
match ;     Force 1 (x)      ; GREPFIELD(static/info, '1        Si', 3) ; -0.118259808
Precision: 8.64e-04
match ;     Force 1 (y)      ; GREPFIELD(static/info, '1        Si', 4) ; -0.08465135780000001
Precision: 6.51e-04
match ;     Force 1 (z)      ; GREPFIELD(static/info, '1        Si', 5) ; -0.0583467098
Precision: 8.99e-05
match ;     Force 2 (x)      ; GREPFIELD(static/info, '2        Si', 3) ; 0.011280548000000001
Precision: 3.26e-05
match ;     Force 2 (y)      ; GREPFIELD(static/info, '2        Si', 4) ; -0.004260716580000001
Precision: 1.10e-05
match ;     Force 2 (z)      ; GREPFIELD(static/info, '2        Si', 5) ; -0.0103611993
Precision: 1.99e-04
match ;     Force 3 (x)      ; GREPFIELD(static/info, '3        Si', 3) ; 0.010587087500000002
Precision: 4.64e-05
match ;     Force 3 (y)      ; GREPFIELD(static/info, '3        Si', 4) ; 0.00825769779
Precision: 2.14e-04
match ;     Force 3 (z)      ; GREPFIELD(static/info, '3        Si', 5) ; 0.00719181529
Precision: 2.14e-05
match ;     Force 4 (x)      ; GREPFIELD(static/info, '4        Si', 3) ; -0.0137859985
Precision: 4.73e-05
match ;     Force 4 (y)      ; GREPFIELD(static/info, '4        Si', 4) ; -0.00451593449
Precision: 8.16e-05
match ;     Force 4 (z)      ; GREPFIELD(static/info, '4        Si', 5) ; 0.00811186593
Precision: 4.21e-04
match ;     Force 5 (x)      ; GREPFIELD(static/info, '5        Si', 3) ; 0.0588360958
Precision: 8.45e-04
match ;     Force 5 (y)      ; GREPFIELD(static/info, '5        Si', 4) ; 0.13715378
Precision: 1.19e-04
match ;     Force 5 (z)      ; GREPFIELD(static/info, '5        Si', 5) ; 0.071435514
Precision: 1.01e-04
match ;     Force 6 (x)      ; GREPFIELD(static/info, '6        Si', 3) ; -0.00139837116
Precision: 4.34e-04
match ;     Force 6 (y)      ; GREPFIELD(static/info, '6        Si', 4) ; 0.009169154430000001
Precision: 2.89e-04
match ;     Force 6 (z)      ; GREPFIELD(static/info, '6        Si', 5) ; -0.0105024008
Precision: 8.10e-05
match ;     Force 7 (x)      ; GREPFIELD(static/info, '7        Si', 3) ; 0.0414722146
Precision: 1.36e-04
match ;     Force 7 (y)      ; GREPFIELD(static/info, '7        Si', 4) ; -0.0410129237
Precision: 1.97e-05
match ;     Force 7 (z)      ; GREPFIELD(static/info, '7        Si', 5) ; 0.04115800620000001
Precision: 4.40e-04
match ;     Force 8 (x)      ; GREPFIELD(static/info, '8        Si', 3) ; 0.0110690613
Precision: 4.98e-04
match ;     Force 8 (y)      ; GREPFIELD(static/info, '8        Si', 4) ; -0.020632260200000002
Precision: 2.77e-04
match ;     Force 8 (z)      ; GREPFIELD(static/info, '8        Si', 5) ; -0.0486925022
